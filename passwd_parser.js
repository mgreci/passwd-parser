#!/usr/bin/env node
//Use fs package
let fs = require("fs");
let services = require("./services");

usage = () => {
  console.log("Invalid or missing arguments. Exiting.");
  console.log("Usage: ./passwd_parser.js /path/to/passwd /path/to/group");
};

//Read command line arguments
//usage: "./passwd_parser.js a b" or "node ./passwd_parser.js a b"
if (process.argv.length != 4) {
  usage();
  process.exit(1);
}

const PASSWD_FILE = process.argv[2];
const GROUP_FILE = process.argv[3];

//allocate final output object
let output = {};

//Confirm filenames are vaild
if (services.confirmFilepaths(PASSWD_FILE, GROUP_FILE) !== true) {
  usage();
  process.exit(2);
}

//Read in passwd information
fs.readFile(PASSWD_FILE, 'utf-8', (passwdError, passwdText) => {
  if (passwdError) {
    console.log("Error while reading passwd file: " + passwdError);
    process.exit(3);
  }
  services.generateUserObject(output, passwdText);
  //Read in group information
  fs.readFile(GROUP_FILE, 'utf-8', (groupError, groupText) => {
    if (groupError) {
      console.log("Error while reading group file: " + groupError);
      process.exit(4);
    }
    //Correlate passwd and group information into single dataset
    services.addGroups(output, groupText);
    //Output (as JSON) each username with uid, full_name, and groups
    console.log(JSON.stringify(output, null, 4));
  });
});
