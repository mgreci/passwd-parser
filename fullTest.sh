#!/bin/bash

#Ensure correct permissions
chmod +x ./passwd_parser.js

# Run program and capture output
./passwd_parser.js ./testData/passwd.txt ./testData/group.txt > output.json

# Compare output json to expected json
diff -q ./testData/expected.json output.json >> /dev/null
RESULT=$(echo $?)

if [ $RESULT != 0 ]; then
  echo "Password Parser is NOT working"
else
  echo "Password Parser is working!"
fi
