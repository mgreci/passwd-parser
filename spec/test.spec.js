let app = require("../services");
describe("Password Parser", () => {
  describe("Handle command line arguments", () => {
    it("should indicate when both arguments are missing", () => {
      const result = app.confirmFilepaths("", "");
      expect(result).toEqual("INVALID_ARGUMENTS");
    });
    it("should indicate when argument A is missing", () => {
      const result = app.confirmFilepaths("", "/etc/group");
      expect(result).toEqual("INVALID_ARGUMENTS");
    });
    it("should indicate when argument B is missing", () => {
      const result = app.confirmFilepaths("/etc/passwd", "");
      expect(result).toEqual("INVALID_ARGUMENTS");
    });
    it("should indicate when arguments are valid", () => {
      const result = app.confirmFilepaths("/etc/password", "/etc/groups");
      expect(result).toBeTruthy();
    });
  });
  describe("Read in Passwords", () => {
    describe("Count Lines", () => {
      it("should count 0 lines", () => {
        let output = {};
        const text = "";
        app.generateUserObject(output, text);
        expect(output).toEqual({});
        expect(Object.keys(output).length).toEqual(0);
      });
      it("should count 2 lines", () => {
        let output = {};
        const text = `a:*:1:1:Name1:/home:bash\nb:*:2:2:Name2:/home:bash\n`;
        app.generateUserObject(output, text);
        expect(Object.keys(output).length).toEqual(2);
      });
      it("should count lines with comments", () => {
        let output = {};
        const text = `a:*:1:1:Name1:/home:bash\n#b:*:2:2:Name2:/home:bash\n`;
        app.generateUserObject(output, text);
        expect(Object.keys(output).length).toEqual(1);
      });
      it("should not count empty lines", () => {
        let output = {};
        const text = `a:*:1:1:Name1:/home:bash\n\nb:*:2:2:Name2:/home:bash\n\n`;
        app.generateUserObject(output, text);
        expect(Object.keys(output).length).toEqual(2);
      });
    });
    describe("Read in Password Information", () => {
      it("should capture uid and name for 1 user", () => {
        let output = {};
        const text = `a:*:1:1:Name1:/home:bash\n`;
        const expected = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : []
          }
        };
        app.generateUserObject(output, text);
        expect(Object.keys(output).length).toEqual(1);
        expect(output).toEqual(expected);
      });
      it("should capture uid and name for 2 users", () => {
        let output = {};
        const text = `a:*:1:1:Name1:/home:bash\nb:*:2:2:Name2:/home:bash\n`;
        const expected = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : []
          },
          "b" : {
            "uid" : "2",
            "full_name" : "Name2",
            "groups" : []
          },
        };
        app.generateUserObject(output, text);
        expect(Object.keys(output).length).toEqual(2);
        expect(output).toEqual(expected);
      });
    });
  });
  describe("Read in Groups", () => {
    describe("Count Lines", () => {
      it("should not modify output with no groups", () => {
        let input = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : []
          }
        };
        const text = "";
        const expected = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : []
          }
        };
        app.addGroups(input, text);
        expect(input).toEqual(expected);
      });
      it("should add group to matching user", () => {
        const input = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : []
          }
        };
        const text = `test:*:1:a\n`;
        const expected = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : ["test"]
          }
        };
        app.addGroups(input, text);
        expect(input).toEqual(expected);
      });
      it("should ignore comments in group file", () => {
        const input = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : []
          }
        };
        const text = `test1:*:1:a\n#test2:*:2:a\ntest3:*:3:a\n`;
        const expected = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : ["test1", "test3"]
          }
        };
        app.addGroups(input, text);
        expect(input).toEqual(expected);
      });
      it("should add multiple groups to multiple matching users", () => {
        const input = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : []
          },
          "b" : {
            "uid" : "2",
            "full_name" : "Name2",
            "groups" : []
          }
        };
        const text = `test1:*:1:a\ntest2:*:2:a,b\ntest3:*:3:a\ntest4:*:4:b\n`;
        const expected = {
          "a" : {
            "uid" : "1",
            "full_name" : "Name1",
            "groups" : ["test1", "test2", "test3"]
          },
          "b" : {
            "uid" : "2",
            "full_name" : "Name2",
            "groups" : ["test2", "test4"]
          }
        };
        const result = app.addGroups(input, text);
        expect(input).toEqual(expected);
      });
    });
  });
});
