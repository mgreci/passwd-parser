exports.confirmFilepaths = (passwdFile,groupFile) => {
  if (passwdFile === "" || groupFile === "") {
    return "INVALID_ARGUMENTS";
  }
  return true;
};

exports.generateUserObject = (output, text) => {
  const lines = text.split('\n');
  if (lines.length > 0) {
    lines.forEach((line) => {
      if (typeof line[0] != "undefined" && line[0] !== "" && line[0] !== "#") {
        array = line.split(':');
        output[array[0]] = {
          "uid" : array[2],
          "full_name" : array[4],
          "groups" : []
        };
      }
    });
  }
};

exports.addGroups = (output, text) => {
  //exit if text is empty
  if (text === "") {
    return;
  }
  //split text into lines
  lines = text.split("\n");
  //for each line
  lines.forEach((line) => {
    //ignore empty lines or comment lines
    if (line !== "" && line[0] !== "#") {
      //split by ':'
      const information = line.split(":");
      //split users of group by ','
      const users = information[3].split(",");
      //for each user
      users.forEach((user) => {
        //if user exists in output object
        if (output.hasOwnProperty(user)) {
          //add group to that user
          output[user].groups.push(information[0]);
        }
      });
    }
  });
};
