# Password Parser
#### Created by Matthew Greci : Demo for Brain Corporation

### Setup
Please ensure Node.js is installed on your system.
```
npm install
```

### Test
#### Unit Testing
```
jasmine
```
#### Full Application Testing
```
./fullTest.sh
```

### Run
```
./passwd_parser.js /path/to/passwd /path/to/group
```
or
```
npm start /path/to/passwd /path/to/group
```
